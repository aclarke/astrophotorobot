package com.clarke.agnes.imaging;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeMap;

public class ImageData {

	public int lowestNoiseCounter;
	public int allNoiseCounter;
	public int badNoiseCounter;
	public int[][] deciPercentileDistribution = new int[1001][4];
	public List<Flatness> flatness = new LinkedList<Flatness>();
	private TreeMap<Integer, Integer> illumination;
	private TreeMap<Integer, Integer> illuminationAverager;
	private final String name;
	private final int iso;
	private final int sec;
	private final File directory;
	private final int temp;
	private ArrayList<ImageData> averagedResults;

	public ImageData(File directory, String name, int iso, int sec, int temp) {
		this.directory = directory;
		this.name = name;
		this.iso = iso;
		this.sec = sec;
		this.temp = temp;
		
	}

	public ImageData(File directory, String name, int iso, int sec,
			int temp, ArrayList<ImageData> dataStack) {
		this.directory = directory;
		this.name = name;
		this.iso = iso;
		this.sec = sec;
		this.temp = temp;
		averagedResults = dataStack;
	}

	public void writeFile() {
		String[] nameParts = name.split("\\.");
		File dataFile = new File(directory, nameParts[0] + ".txt");
		try {
			FileWriter writer = new FileWriter(dataFile);
			StringBuilder sb = new StringBuilder();
			sb.append("" + directory.getAbsolutePath() + File.separator + name + '\n');
			sb.append("lowestNoiseCounter: " + lowestNoiseCounter + '\n');
			sb.append("allNoiseCounter: " + allNoiseCounter + '\n');
			sb.append("badNoiseCounter: " + badNoiseCounter + '\n');
			int percentile = 0;
			sb.append("%Illum\tR\tG\tB\tM\n");
			for (int[] rgbm : deciPercentileDistribution) {
				sb.append(percentile++ + "\t" + rgbm[0] + "\t" + rgbm[1] + "\t" + rgbm[2] + "\t" + rgbm[3] + '\n');
			}
			sb.append("%Dist\tR\tG\tB\tM\n");
			for (Flatness f : flatness) {
				sb.append(f.distanceToCorner + "\t" + f.percentCenter + "\t" + f.samples + '\n');
			}
			writer.write(sb.toString());
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}

	public void setDeciPercentileDistribution(int percent, int red, int green, int blue, int mono) {
		deciPercentileDistribution[percent][0] = red;
		deciPercentileDistribution[percent][1] = green;
		deciPercentileDistribution[percent][2] = blue;
		deciPercentileDistribution[percent][3] = mono;
		
	}
	
	public int getDeciPercentileDistributionRange(int channel, int rangeStartInclusive, int rangeEndInclusive) {
		if (averagedResults == null) {
			int result = 0;
			for (int i = rangeStartInclusive; i <= rangeEndInclusive; i++) {
				result += deciPercentileDistribution[i][channel];
			}
			return result;
		} else {
			int result = 0;
			for (ImageData datum : averagedResults) {
				result += datum.getDeciPercentileDistributionRange(channel, rangeStartInclusive, rangeEndInclusive);
			}
			return result / averagedResults.size();
		}
	}

	public void setIllumination(TreeMap<Integer, Integer> illumination,
			TreeMap<Integer, Integer> illuminationAverager) {
				this.illumination = illumination;
				this.illuminationAverager = illuminationAverager;
				setFieldFlatness();
	}
	
	private void setFieldFlatness() {
		int maxIllumination = 0;
		for (int i = 0; i < 101; i++) {
			int illum = illumination.get(i);
			int avg = illuminationAverager.get(i);
			int candidate = illum / avg;
			maxIllumination = maxIllumination > candidate ? maxIllumination : candidate;
		}
		for (int i = 0; i < 101; i++) {
			Integer currentIllumination = illumination.get(i);
			Integer samples = illuminationAverager.get(i);
			int percentCenter = (currentIllumination * 100 / samples) / maxIllumination;
			flatness.add(new Flatness(i, percentCenter, samples));
		}
	}
	
	public int getIso() {
		return iso;
	}

	public int getSec() {
		return sec;
	}

	public int getTemp() {
		return temp;
	}
	
	public boolean hasTempData() {
		return temp != -99;
	}

	private static class Flatness {
		private final int distanceToCorner;
		private final int percentCenter;
		private final int samples;

		private Flatness(int distanceToCorner, int percentCenter, int samples) {
			this.distanceToCorner = distanceToCorner;
			this.percentCenter = percentCenter;
			this.samples = samples;
		}
	}

	public boolean isMedian() {
		return name.equals("median");
	}

	public String name() {
		return name.equals("median") ? "median" : name.equals("average") ? "average" : "";
	}
}
