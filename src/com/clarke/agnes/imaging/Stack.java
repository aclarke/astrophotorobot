package com.clarke.agnes.imaging;

import net.sourceforge.jiu.data.MemoryRGB48Image;
import net.sourceforge.jiu.data.PixelImage;

public class Stack {
	private int imagesInStack;
	private MemoryRGB48Image data;
	
	public Stack(PixelImage baseImage) {
		data = new MemoryRGB48Image(baseImage.getWidth(), baseImage.getHeight());
		stack(baseImage);
	}
	
	public PixelImage average() {
		int width = data.getWidth();
		int height = data.getHeight();
		MemoryRGB48Image result = new MemoryRGB48Image(width, height);
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < width; y++) {
				int red = data.getSample(0, x, y);
				int green = data.getSample(1, x, y);
				int blue = data.getSample(2, x, y);
				result.putSample(0, x, y, red / imagesInStack);
				result.putSample(1, x, y, green / imagesInStack);
				result.putSample(2, x, y, blue / imagesInStack);
			}
		}
		return result;
	}

	private void stack(PixelImage image) {
		int width = data.getWidth();
		int height = data.getHeight();
		if (image.getWidth() != width || image.getHeight() != height) {
			throw new RuntimeException(String.format("Image dimensions do not match. Expecting %s,%s, found %s,%s.", 
					width, height, image.getWidth(), image.getHeight()));
		}
		imagesInStack++;
		if (imagesInStack > 30000) {
			throw new RuntimeException("Too many images in stack, exceding 30,000.");
		}
		MemoryRGB48Image daImage = (MemoryRGB48Image) image;
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < width; y++) {
				int red = daImage.getSample(0, x, y);
				int green = daImage.getSample(1, x, y);
				int blue = daImage.getSample(2, x, y);
				int stackedRed = data.getSample(0, x, y) + red;
				int stackedGreen = data.getSample(1, x, y) + green;
				int stackedBlue = data.getSample(2, x, y) + blue;
				data.putSample(0, x, y, stackedRed);
				data.putSample(1, x, y, stackedGreen);
				data.putSample(2, x, y, stackedBlue);
			}
		}
	}

}
