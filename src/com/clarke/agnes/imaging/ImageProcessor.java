package com.clarke.agnes.imaging;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeMap;

import net.sourceforge.jiu.codecs.CodecMode;
import com.clarke.agnes.imaging.TIFFCodec;
import net.sourceforge.jiu.data.MemoryRGB48Image;
import net.sourceforge.jiu.data.PixelImage;
import net.sourceforge.jiu.ops.MissingParameterException;
import net.sourceforge.jiu.ops.OperationFailedException;

public class ImageProcessor {
	
	public static void main(String[] args) throws IOException, MissingParameterException, OperationFailedException {
		File directory = new File("C:\\Users\\agslinda\\Pictures\\gina");
		List<ImageData> results = new LinkedList<ImageData>();
		for (File tiff : directory.listFiles()) {
			if (tiff.getName().endsWith(".tiff")) {
				String name = tiff.getName();
				System.out.println(name);
				boolean followsNamingConvention = name.startsWith("dark");
				String[] tiffBits = name.split("_");
				int sec = followsNamingConvention ? Integer.parseInt(tiffBits[1].replace("sec", "")) : 0;
				int iso = followsNamingConvention ? Integer.parseInt(tiffBits[2].replace("iso", "")
						.replace(".tiff", "")) : 0;
				int temp = -99;
				if (tiffBits.length == 4) {
					temp = Integer.parseInt(tiffBits[3].replace("temp", "").replace(".tiff", ""));
				}
				try {
					TIFFCodec codec = new TIFFCodec();
					codec.setFile(tiff, CodecMode.LOAD);
					codec.process();
					PixelImage image = codec.getImage();
					ImageData data = analyzeImage(directory, name, iso, sec, temp, tiff,
							image);
					data.writeFile();
					results.add(data);
				} catch (Exception e) {
					System.out.println("Could not process file: " + name + " because " + e.getMessage());
				}
				Collections.sort(results, new ImageDataComparator());
			}
		}
		System.out.println("ISO,SECONDS,TEMPERATURE,ALL_NOISE,LOWEST_NOISE,BAD_NOISE");
		for (ImageData datum : results) {
			System.out.println(datum.getIso() + "," + datum.getSec() + "," + datum.getTemp() + "," + datum.allNoiseCounter + "," + datum.lowestNoiseCounter + "," + datum.badNoiseCounter);
		}
		System.out.println("ISO,SECONDS,TEMPERATURE,0%,1-10%,11-40%,41-100%,1-100%");
		for (ImageData datum : results) {
			System.out.print(datum.getIso() + "," + datum.getSec() + "," + datum.getTemp() + ",");
			System.out.print(datum.getDeciPercentileDistributionRange(3, 0, 0) + ",");
			System.out.print(datum.getDeciPercentileDistributionRange(3, 1, 100) + ",");
			System.out.print(datum.getDeciPercentileDistributionRange(3, 101, 400) + ",");
			System.out.print(datum.getDeciPercentileDistributionRange(3, 401, 1000) + ",");
			System.out.print(datum.getDeciPercentileDistributionRange(3, 1, 1000));
			System.out.println();
		}
		System.out.println("ISO,SECONDS,TEMPERATURE,1-2%,3-4%,5-8%,9-16%,17-64%,65-100%");
		for (ImageData datum : results) {
			System.out.print(datum.getIso() + "," + datum.getSec() + "," + datum.getTemp() + ",");
			System.out.print(datum.getDeciPercentileDistributionRange(0, 1, 20) + ",");
			System.out.print(datum.getDeciPercentileDistributionRange(0, 21, 40) + ",");
			System.out.print(datum.getDeciPercentileDistributionRange(0, 41, 80) + ",");
			System.out.print(datum.getDeciPercentileDistributionRange(0, 81, 160) + ",");
			System.out.print(datum.getDeciPercentileDistributionRange(0, 161, 640) + ",");
			System.out.print(datum.getDeciPercentileDistributionRange(0, 641, 1000));
			System.out.println();
		}
		System.out.println("ISO,SECONDS,TEMPERATURE,0.1%,0.3%,0.6%,1%,2%,3%");
		for (ImageData datum : results) {
			System.out.print(datum.getIso() + "," + datum.getSec() + "," + datum.getTemp() + ",");
			System.out.print(datum.getDeciPercentileDistributionRange(0, 0, 1) + ",");
			System.out.print(datum.getDeciPercentileDistributionRange(0, 2, 3) + ",");
			System.out.print(datum.getDeciPercentileDistributionRange(0, 4, 6) + ",");
			System.out.print(datum.getDeciPercentileDistributionRange(0, 7, 10) + ",");
			System.out.print(datum.getDeciPercentileDistributionRange(0, 11, 20) + ",");
			System.out.print(datum.getDeciPercentileDistributionRange(0, 21, 30));
			System.out.println();
		}
	}
	
	public static ImageData analyzeImage(File directory, String name, int iso, int sec, int temp, File file, PixelImage image) {
		ImageData data = new ImageData(directory, name, iso, sec, temp);
		TreeMap<Integer,Integer> rDistribution = new TreeMap<Integer,Integer>();
		TreeMap<Integer,Integer> gDistribution = new TreeMap<Integer,Integer>();
		TreeMap<Integer,Integer> bDistribution = new TreeMap<Integer,Integer>();
		TreeMap<Integer,Integer> mDistribution = new TreeMap<Integer,Integer>();
		
		if (image.getBitsPerPixel() == 48) {
			MemoryRGB48Image daImage = (MemoryRGB48Image) image;
			int height = image.getHeight();
			int width = image.getWidth();
			int lowestNoiseCounter = 0;
			int allNoiseCounter = 0;
			int badNoiseCounter = 0;
			for (int x = 0; x < width; x++) {
				for (int y = 0; y < height; y++) {
					int rSample = daImage.getSample(0, x, y);
					int r = rSample / (Short.MAX_VALUE / 500);
					int gSample = daImage.getSample(1, x, y);
					int g = gSample / (Short.MAX_VALUE / 500);
					int bSample = daImage.getSample(2, x, y);
					int b = bSample / (Short.MAX_VALUE / 500);
					int m = (r + g + b) / 3;
					lowestNoiseCounter = lowestNoise(iso, lowestNoiseCounter, rSample, gSample, bSample);
					if (rSample > 0 || gSample > 0 || bSample > 0) {
						allNoiseCounter++;
					}
					if (rSample > Short.MAX_VALUE || gSample > Short.MAX_VALUE || bSample > Short.MAX_VALUE) {
						badNoiseCounter++;
					}
					int rCount = rDistribution.get(r) == null ? 1 : rDistribution.get(r) + 1;
					int gCount = gDistribution.get(g) == null ? 1 : gDistribution.get(g) + 1;
					int bCount = bDistribution.get(b) == null ? 1 : bDistribution.get(b) + 1;
					int mCount = mDistribution.get(m) == null ? 1 : mDistribution.get(m) + 1;
					rDistribution.put(r, rCount);
					gDistribution.put(g, gCount);
					bDistribution.put(b, bCount);
					mDistribution.put(m, mCount);
				}
			}
			data.lowestNoiseCounter = lowestNoiseCounter;
			data.allNoiseCounter = allNoiseCounter;
			data.badNoiseCounter = badNoiseCounter;
			for (int i = 0; i < 1001; i++) {
				data.setDeciPercentileDistribution(i, 
						denull(rDistribution.get(i)), 
						denull(gDistribution.get(i)), 
						denull(bDistribution.get(i)), 
						denull(mDistribution.get(i)));
			}
			
			//field illumination
			TreeMap<Integer,Integer> illumination = new TreeMap<Integer,Integer>();
			TreeMap<Integer,Integer> illuminationAverager = new TreeMap<Integer,Integer>();
			for (int x = 0; x < width; x++) {
				int y = height * x / width;
				int x1 = width - x - 1;
				int r = (daImage.getSample(0, x, y));
				int g = (daImage.getSample(1, x, y));
				int b = (daImage.getSample(2, x, y));
				int m = (r + g + b) / 3;
				int r1 = (daImage.getSample(0, x1, y));
				int g1 = (daImage.getSample(1, x1, y));
				int b1 = (daImage.getSample(2, x1, y));
				int m1 = (r1 + g1 + b1) / 3;
				int distanceFromCenter = distanceFromCenter(x, width);
				int mIlluminationAverager = illuminationAverager.get(distanceFromCenter) == null ? 2 : illuminationAverager.get(distanceFromCenter) + 2;
				int mIllumination = illumination.get(distanceFromCenter) == null ? m + m1 : illumination.get(distanceFromCenter) + m + m1;
				illuminationAverager.put(distanceFromCenter, mIlluminationAverager);
				illumination.put(distanceFromCenter, mIllumination);
			}
			data.setIllumination(illumination, illuminationAverager);
		}
		return data;
	}

	private static int denull(Integer integer) {
		return integer == null ? 0 : integer;
	}

	public static int lowestNoise(int ISO, int lowestNoiseCounter, int r, int g, int b) {
		int sample = r > g ? r : g;
		sample = sample > b ? sample : b;
		if (sample <= (32 * (ISO / 100)) && sample != 0) {
			lowestNoiseCounter++;
		}
		return lowestNoiseCounter;
	}

	public static int distanceFromCenter(int x, int width) {
		return Math.abs(((200 * x) / width) - 100);
	}


	
}
