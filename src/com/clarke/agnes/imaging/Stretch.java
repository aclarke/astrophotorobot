package com.clarke.agnes.imaging;

import net.sourceforge.jiu.data.MemoryRGB48Image;

public class Stretch extends ImageMaker {
	
	private final double stretchFactor;

	public Stretch(double stretchFactor) {
		this.stretchFactor = stretchFactor <= 2 ? stretchFactor : 2;
	}

	@Override
	public MemoryRGB48Image process(MemoryRGB48Image input) {
		StretchAction action = new StretchAction(stretchFactor);
		MemoryRGB48Image processTemplate = processTemplate(input, action);
		return processTemplate;
	}
	
	private static class StretchAction implements ImageMakerAction {

		private final double stretchFactor;
		private static final int MAX = Short.MAX_VALUE * 2;

		public StretchAction(double stretchFactor) {
			this.stretchFactor = stretchFactor;
		}

		@Override
		public void perform(int x, int y, MemoryRGB48Image input,
				MemoryRGB48Image result) {
			for (int channel = 0; channel < 3; channel ++) {
				int sample = input.getSample(channel, x, y);
				double stretch = ((double)(stretchFactor - 1) * ((double)(MAX - sample) / MAX)) + 1;
				result.putSample(channel, x, y, (int) (stretch * sample));
			}
		}
		
	}

}
