package com.clarke.agnes.imaging;

import net.sourceforge.jiu.data.MemoryRGB48Image;

public class AverageAllPixels extends ImageMaker {
	
	@Override
	public MemoryRGB48Image process(MemoryRGB48Image input) {
		AverageAllPixelsAction action = new AverageAllPixelsAction();
		MemoryRGB48Image result = processTemplate(input, action);
		GenerateAverageImageAction action2 = new GenerateAverageImageAction(action.getTotalBrightness(), action.getTotalPixels());
		return processTemplate(result, action2);
	}
	
	private static class AverageAllPixelsAction implements ImageMakerAction {
		
		private long totalBrightness[] = {0, 0, 0};
		private long totalPixels = 0;

		@Override
		public void perform(int x, int y, MemoryRGB48Image input,
				MemoryRGB48Image result) {
			totalBrightness[0] += input.getSample(0, x, y);
			totalBrightness[1] += input.getSample(1, x, y);
			totalBrightness[2] += input.getSample(2, x, y);
			totalPixels++;
		}

		public long[] getTotalBrightness() {
			return totalBrightness;
		}

		public long getTotalPixels() {
			return totalPixels;
		}
	}
	
	private static class GenerateAverageImageAction implements ImageMakerAction {
		private int red;
		private int green;
		private int blue;

		public GenerateAverageImageAction(long[] totalBrightness, long totalPixels) {
			red = (int) (totalBrightness[0] / totalPixels);
			green = (int) (totalBrightness[1] / totalPixels);
			blue = (int) (totalBrightness[2] / totalPixels);
		}

		@Override
		public void perform(int x, int y, MemoryRGB48Image input,
				MemoryRGB48Image result) {
			result.putSample(0, x, y, red);
			result.putSample(1, x, y, green);
			result.putSample(2, x, y, blue);
			
		}
		
	}

}
