package com.clarke.agnes.imaging;

import net.sourceforge.jiu.data.MemoryRGB48Image;

public abstract class ImageMaker {
	
	protected MemoryRGB48Image processTemplate(MemoryRGB48Image input, ImageMakerAction action) {
		int width = input.getWidth();
		int height = input.getHeight();
		
		MemoryRGB48Image result = new MemoryRGB48Image(width, height);
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				progress();
				action.perform(x, y, input, result);
			}
		}
		System.out.println("!\n");
		return result;
	}
	
	private int progressCounter = 0;
	private void progress() {
		progressCounter++;
		if (progressCounter % 400000 == 0) {
			System.out.print('.');
		}
		
	}
	public abstract MemoryRGB48Image process(MemoryRGB48Image input);
	
	protected static interface ImageMakerAction {
		void perform(int x, int y, MemoryRGB48Image input, MemoryRGB48Image result);
	}

}
