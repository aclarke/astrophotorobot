package com.clarke.agnes.imaging;

import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class DataToGraphs {
	
	public static void main(String[] args) throws IOException {
		FileReader reader = new FileReader("C:\\Users\\agslinda\\Documents\\gina3.csv");
		StringBuilder sb = new StringBuilder();
		LinkedList<Result> results = new LinkedList<Result>();
		int ch = reader.read();
		while (ch != -1) {
			sb.append((char)ch);
			ch = reader.read();
		}
		String[] lines = sb.toString().split("\n");
		for (int i = 1; i < lines.length; i++) {
			boolean median;
			int iso, sec, temp;
			int[] ia = new int[16];
			String[] cells = lines[i].trim().split(",");
			median = cells[0].equals("median");
			iso = Integer.valueOf(cells[1]);
			sec = Integer.valueOf(cells[2]);
			temp = Integer.valueOf(cells[3]);
			for (int k = 4; k < cells.length; k++) {
				ia[k - 4] = Integer.valueOf(cells[k]);
			}
			Result result = new Result(median, iso, sec, temp, ia);
			results.add(result);
		}
		boolean doMore;
		Result matchMe = null;
		LinkedList<Result> medianGroup = new LinkedList<Result>();
		LinkedList<Result> avgGroup = new LinkedList<Result>();
		//Graphs for constant iso and sec
		/*while (doMore) {
			for (Result r : results) {
				if (!r.done && matchMe == null) {
					matchMe = r;
				}
				if (!r.done && matchMe.iso == r.iso && matchMe.sec == r.sec) {
					r.done = true;
					if (r.median == true) {
						medianGroup.add(r);
					} else {
						avgGroup.add(r);
					}
				}
			}
			graph(medianGroup, "Temp", 4);
			graph(avgGroup, "Temp", 4);
			matchMe = null;
			medianGroup.clear();
			avgGroup.clear();
			doMore = false;
			for (Result r : results) {
				if (!r.done) {
					doMore = true;
					break;
				}
			}
		}*/
		doMore = reset(results);
		//Graphs for constant iso and temp
		while (doMore) {
			for (Result r : results) {
				if (!r.done && matchMe == null) {
					matchMe = r;
				}
				if (!r.done && matchMe.iso == r.iso && matchMe.temp == r.temp) {
					r.done = true;
					if (r.median) {
						medianGroup.add(r);
					} else {
						avgGroup.add(r);
					}
				}
			}
			graph(medianGroup, "Sec", 4);
			graph(avgGroup, "Sec", 4);
			matchMe = null;
			medianGroup.clear();
			avgGroup.clear();
			doMore = false;
			for (Result r : results) {
				if (!r.done) {
					doMore = true;
					break;
				}
			}
		}
		//Graphs for constant sec and temp
		/*while (doMore) {
			for (Result r : results) {
				if (!r.done && matchMe == null) {
					matchMe = r;
				}
				if (!r.done && matchMe.sec == r.sec && matchMe.temp == r.temp) {
					r.done = true;
					if (r.median == true) {
						medianGroup.add(r);
					} else {
						avgGroup.add(r);
					}
				}
			}
			graph(medianGroup, "ISO", 4);
			graph(avgGroup, "ISO", 4);
			matchMe = null;
			medianGroup.clear();
			avgGroup.clear();
			doMore = false;
			for (Result r : results) {
				if (!r.done) {
					doMore = true;
					break;
				}
			}
		}*/
	}

	public static boolean reset(LinkedList<Result> results) {
		for (Result r : results) {
			r.done = false;
		}
		return true;
	}
	
	public static void graph(List<Result> group, String label, int floor) {
		Result aResult = group.get(0);
		String title;
		if (label.equals("Temp")) {
			title = (aResult.median ? "Median " : "Average ") + "ISO" + aResult.iso + ", " + aResult.sec + " seconds";
		} else if (label.equals("Sec")) {
			title = (aResult.median ? "Median " : "Average ") + "ISO" + aResult.iso + ", " + aResult.temp + "C";
		} else if (label.equals("ISO")) {
			title = (aResult.median ? "Median " : "Average ") + aResult.sec + " seconds, " + aResult.temp + "C";
		} else {
			title = "missing title";
		}
		StringBuilder sb = new StringBuilder();
		sb.append("<p><b>").append(title).append("</b></p>\n");
		sb.append("<table>\n");
		for (Result r : group) {
			int xAxis = 0; 
			if ("Temp".equals(label)) xAxis = r.temp;
			if ("Sec".equals(label)) xAxis = r.sec;
			if ("ISO".equals(label)) xAxis = r.iso;
			sb.append("<tr><td>").append(xAxis).append("</td><td>");
			int total = 0;
			StringBuilder sb2 = new StringBuilder();
			for (int i = floor; i < r.values.length; i++) {
				total += r.values[i];
				sb2.append("<span style=\"font-size:9px;background:rgb(").append(100 + i * 8).append(", 64, 64)\">");
				for (int x = 10; x < r.values[i]; x += 10) {
					sb2.append("&nbsp;");
				}
				sb2.append("</span>");
			}
			if (total < 4000) {
				sb.append(sb2);
			}
			sb.append(total);
			sb.append("</td></tr>\n");
		}
		sb.append("</table>\n");
		System.out.println(sb.toString());
	}
	
	public static class Result {
		public final boolean median;
		public final int iso;
		public final int sec;
		public final int temp;
		public final int[] values;
		public boolean done = false;
		public Result(boolean median, int iso, int sec, int temp, int[] values) {
			this.median = median;
			this.iso = iso;
			this.sec = sec;
			this.temp = temp;
			this.values = values;
			
		}
	}

}
