package com.clarke.agnes.imaging;


import net.sourceforge.jiu.data.MemoryRGB48Image;

public class GradientRemover extends ImageMaker {

	@Override
	public MemoryRGB48Image process(MemoryRGB48Image input) {
		GradientRemoverGradientCalculator calculateGradient = new GradientRemoverGradientCalculator(16, 16, input);
		processTemplate(input, calculateGradient);
		GradientRemoverGradientBuilder drawGradient = new GradientRemoverGradientBuilder(calculateGradient);
        return processTemplate(input, drawGradient);
	}
	
	private static class GradientRemoverGradientBuilder implements ImageMakerAction {

		private final GradientRemoverGradientCalculator calculateGradient;

		public GradientRemoverGradientBuilder(
				GradientRemoverGradientCalculator calculateGradient) {
					this.calculateGradient = calculateGradient;
		}

		@Override
		public void perform(int x, int y, MemoryRGB48Image input,
				MemoryRGB48Image result) {
			int blockW = x / calculateGradient.blockPixelsWide;
			int blockH = y / calculateGradient.blockPixelsHigh;
			BlockValues currentBlock = calculateGradient.blockValues[blockW][blockH];
			int displacementW = (int) (x - (((float)blockW + 0.5) * calculateGradient.blockPixelsWide));
			int displacementH = (int) (y - (((float)blockH + 0.5) * calculateGradient.blockPixelsHigh));
			BlockValues beforeAfterBlock = null;
			BlockValues aboveBelowBlock = null;
			BlockValues oppositeCornerBlock = null;
			if (displacementW != 0) {
				if (displacementW < 0) {
					if (blockW != 0) {
						beforeAfterBlock = calculateGradient.blockValues[blockW - 1][blockH];
					} else {
						beforeAfterBlock = new ReflectedBlock(calculateGradient.blockValues[blockW][blockH], calculateGradient.blockValues[blockW + 1][blockH]);
					}
				} else if (displacementW > 0) {
					if (blockW < calculateGradient.blocksWide - 1) {
						beforeAfterBlock = calculateGradient.blockValues[blockW + 1][blockH];
					} else {
						beforeAfterBlock = new ReflectedBlock(calculateGradient.blockValues[blockW][blockH], calculateGradient.blockValues[blockW - 1][blockH]);
					}
				}
			}
			if (displacementH != 0) {
				if (displacementH < 0) {
					if (blockH != 0) {
						aboveBelowBlock = calculateGradient.blockValues[blockW][blockH - 1];
					} else {
						aboveBelowBlock = new ReflectedBlock(calculateGradient.blockValues[blockW][blockH], calculateGradient.blockValues[blockW][blockH + 1]);
					}
				} else if (displacementH > 0) {
					if (blockH < calculateGradient.blocksHigh - 1) {
						aboveBelowBlock = calculateGradient.blockValues[blockW][blockH + 1];
					} else {
						aboveBelowBlock = new ReflectedBlock(calculateGradient.blockValues[blockW][blockH], calculateGradient.blockValues[blockW][blockH - 1]);
					}
				}
			}
			if (beforeAfterBlock != null && aboveBelowBlock != null) {
				oppositeCornerBlock = calculateGradient.blockValues[beforeAfterBlock.blockW][aboveBelowBlock.blockH];
			}
			for (int ch = 0; ch < 3; ch++) {
				int blended = blend(currentBlock, beforeAfterBlock, aboveBelowBlock, oppositeCornerBlock, x, y, ch);
				result.putSample(ch, x, y, blended);
			}
			
		}

		private int blend(BlockValues currentBlock,
				BlockValues beforeAfterBlock, BlockValues aboveBelowBlock,
				BlockValues oppositeCornerBlock, int x, int y, int channel) {
			double[][] values = new double[4][2];
			double valueCurrent = currentBlock.getLowest(channel);
			double distanceCurrent = hypotenuse(currentBlock, x, y);
			values[0][0] = distanceCurrent;
			values[0][1] = valueCurrent;
			if (distanceCurrent < 0.01) {
				return (int) valueCurrent;
			}
			BlockValues[] bvs = {beforeAfterBlock, aboveBelowBlock, oppositeCornerBlock};
			int counter = 0;
			for (BlockValues bv : bvs) {
				counter++;
				if (bv != null) {
					double val = bv.getLowest(channel);
					double dist = hypotenuse(bv, x, y);
					values[counter][0] = dist;
					values[counter][1] = val;
				} else {
					values[counter][0] = 9999999;
				}
			}
			double totalWeight = 0;
			double totalValue = 0;
			for (int i = 0; i < 3; i++) {
				if (values[i][0] != 9999999 && notFourthMostDistant(i, values)) {
					double weight = 100.0 / values[i][0];
					totalValue += values[i][1] * weight;
					totalWeight += weight;
				}
			}
			return (int) (totalValue / totalWeight);
		}

		private boolean notFourthMostDistant(int i, double[][] values) {
			double checkMe = values[i][0];
			int imBiggerThanYou = 0;
			for (int f = 0; f < 4; f++) {
				if (f != i && values[f][0] != 999999) {
					if (values[f][0] < checkMe) {
						imBiggerThanYou++;
					}
				}
			}
			return imBiggerThanYou != 3;
		}

		public double getWeight(BlockValues currentBlock, int x, int y, double distanceDivider) {
			double distanceToCurrent = hypotenuse(currentBlock, x, y);
			double currentWeight = (distanceDivider - distanceToCurrent) / distanceDivider;
			currentWeight = currentWeight < 0 ? 0 : currentWeight;
			return currentWeight;
		}

		public double hypotenuse(BlockValues currentBlock, int x, int y) {
			return Math.sqrt(Math.pow(x - currentBlock.getW(), 2) + Math.pow(y - currentBlock.getH(), 2));
		}
		
	}
	
	private static class GradientRemoverGradientCalculator implements ImageMakerAction {
		
		private final int blocksWide;
		private final int blocksHigh;
		private final int blockPixelsWide;
		private final int blockPixelsHigh;
		private final BlockValues[][] blockValues;

		private GradientRemoverGradientCalculator(int blocksWide, int blocksHigh, MemoryRGB48Image input) {
			this.blocksWide = blocksWide;
			this.blocksHigh = blocksHigh;
		    blockPixelsWide = (input.getWidth() / blocksWide) + 1;  
		    blockPixelsHigh = (input.getHeight() / blocksHigh) + 1; 
		    blockValues = new BlockValues[blocksWide][blocksHigh];
			for (int x = 0; x < blocksWide; x++) {
				for (int y = 0; y < blocksHigh; y++) {
					blockValues[x][y] = new BlockValues(x, y, blockPixelsWide, blockPixelsHigh);
				}
			}
		}

		@Override
		public void perform(int x, int y, MemoryRGB48Image input,
				MemoryRGB48Image result) {
			if (x > (input.getWidth() - 3) || y > (input.getHeight() - 3)) {
				return; //binning
			}
			if (x == 0 || y == 0) {
				return; //binning
			}
			int blockW = x / blockPixelsWide;
			int blockH = y / blockPixelsHigh;
			BlockValues bv = blockValues[blockW][blockH];
			for (int ch = 0; ch < 3; ch++) {
				int binned = bin(0, x, y, input) / 4;
				bv.addPoint(x, y, ch, binned);
			}
		}

		public int bin(int channel, int x, int y, MemoryRGB48Image input) {
			return input.getSample(channel, x, y)
			+ input.getSample(0, x+1, y)
			+ input.getSample(0, x, y+1)
			+ input.getSample(0, x+1, y+1);
		}
		
		
	}
	
	public static class BlockValues {
		private GradientPoint[] rPoints = new GradientPoint[5];
		private GradientPoint[] gPoints = new GradientPoint[5];
		private GradientPoint[] bPoints = new GradientPoint[5];
		private GradientPoint[][] channels = {rPoints, gPoints, bPoints};
		private final int blockW;
		private final int blockH;
		private final int pixelsW;
		private final int pixelsH;
		
		public BlockValues(int blockW, int blockH, int pixelsW, int pixelsH) {
			this.blockW = blockW;
			this.blockH = blockH;
			this.pixelsW = pixelsW;
			this.pixelsH = pixelsH;
		}
		
		public int getW() {
			return (blockW * pixelsW) + (pixelsW / 2);
		}
		
		public int getH() {
			return (blockH * pixelsH) + (pixelsH / 2);
		}
		
		public void addPoint(int x, int y, int channel, int binnedValue) {
			GradientPoint[] points = channels[channel];
			int counter = 0;
			int insertionPoint = 0;
			GradientPoint newPoint = null;
			for (GradientPoint gp : points) {
				if (gp == null || gp.binnedValue > binnedValue) {
					insertionPoint = counter;
					newPoint = new GradientPoint(x, y, binnedValue);
					break;
				}
				counter++;
			}
			if (newPoint != null) {
				points[insertionPoint] = newPoint;
			}
		}
		public int getLowest(int channel) {
			int result = 10000000;
			for (GradientPoint gp : channels[channel]) {
				result = result < gp.binnedValue ? result : gp.binnedValue;
			}
			return result;
		}
	}
	
	private static class GradientPoint {
		final int x;
		final int y;
		final int binnedValue;
		private GradientPoint(int x, int y, int binnedValue) {
			this.x = x;
			this.y = y;
			this.binnedValue = binnedValue;
		}
	}
}
