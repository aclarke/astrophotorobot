package com.clarke.agnes.imaging;

import java.util.Comparator;

final class ImageDataComparator implements
		Comparator<ImageData> {
	@Override
	public int compare(ImageData o1, ImageData o2) {
		int o1iso = o1.getIso();
		int o1sec = o1.getSec();
		int o1temp = o1.getTemp();
		int o2iso = o2.getIso();
		int o2sec = o2.getSec();
		int o2temp = o2.getTemp();
		int higherIso = o1iso - o2iso;
		int higherSec = o1sec - o2sec;
		int higherTemp = o1temp - o2temp;
		if (higherIso == 0 && higherSec == 0 && higherTemp == 0) {
			return 0;
		}
		if (higherIso != 0) {
			return higherIso > 0 ? 1 : -1;
		}
		if (higherSec != 0) {
			return higherSec > 0 ? 1 : -1;
		}
		return higherTemp > 0 ? 1 : -1;
	}
}