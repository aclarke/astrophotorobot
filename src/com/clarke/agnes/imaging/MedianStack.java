package com.clarke.agnes.imaging;

import java.util.Arrays;

import net.sourceforge.jiu.data.MemoryRGB48Image;

public class MedianStack extends ImageMaker {
	
	private final MemoryRGB48Image[] stack;
	
	public MedianStack(MemoryRGB48Image[] stack) {
		this.stack = stack;
	}

	@Override
	public MemoryRGB48Image process(MemoryRGB48Image input) {
		if (input != null) {
			throw new RuntimeException("MedianStack does not require an input image - pass the stack in the constructor");
		}
		StackAction action = new StackAction(stack);
		MemoryRGB48Image result = processTemplate(stack[0], action);
		return result;
	}
	
	private static class StackAction implements ImageMakerAction {
		
		private final MemoryRGB48Image[] stack;
		private final int stackSize;
		private final int[] pixelStack;

		private StackAction(MemoryRGB48Image[] stack) {
			this.stack = stack;
			stackSize = stack.length;
			pixelStack = new int[stackSize];
			
		}

		@Override
		public void perform(int x, int y, MemoryRGB48Image input,
				MemoryRGB48Image result) {
			for (int channel = 0; channel < 3; channel++) {
				int position = 0;
				for (MemoryRGB48Image fromStack : stack) {
					pixelStack[position] = fromStack.getSample(channel, x, y);
					position++;
				}
				int pixel = 0;
				if (stackSize == 1) {
					pixel = pixelStack[0];
				} else if (stackSize == 2) {
					pixel = (pixelStack[0] + pixelStack[1]) / 2;
				} else {
					pixel = median(pixelStack);
				}
				result.putSample(channel, x, y, pixel);
			}
			
		}

		private int median(int[] pixels) {
			if (pixels.length < 3) {
				throw new RuntimeException("Median requires three values");
			}
			Arrays.sort(pixels);
			int sum = 0;
			for (int i = 1; i < pixels.length - 1; i++) {
				sum += pixels[i];
			}
			return sum / (pixels.length - 2);
		}
		
	}

}
