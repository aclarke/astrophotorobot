package com.clarke.agnes.imaging;

import net.sourceforge.jiu.data.MemoryRGB48Image;
import net.sourceforge.jiu.data.MemoryShortChannelImage;
import net.sourceforge.jiu.data.PixelImage;

public class Subtract extends ImageMaker {
	
	private final MemoryRGB48Image subtraction;
	private final double scalingFactor;
	private final boolean correctLuminosity;

	public Subtract(MemoryRGB48Image subtraction, double scalingFactor, boolean correctLuminosity) {
		this.subtraction = subtraction;
		this.scalingFactor = scalingFactor;
		this.correctLuminosity = correctLuminosity;
	}

	@Override
	public MemoryRGB48Image process(MemoryRGB48Image input) {
		SubtractAction action = new SubtractAction(subtraction, correctLuminosity, scalingFactor);
		MemoryRGB48Image result = processTemplate(input, action);
		return result;
	}
	
	private static class SubtractAction implements ImageMakerAction {

		private final int MAXVALUE = Short.MAX_VALUE * 2;
		private int width;
		private int height;
		private final MemoryRGB48Image subtraction;
		private final boolean correctLuminosity;
		private final double scalingFactor;

		private SubtractAction(MemoryRGB48Image subtraction, boolean correctLuminosity, double scalingFactor) {
			this.subtraction = subtraction;
			this.correctLuminosity = correctLuminosity;
			this.scalingFactor = scalingFactor;
			width = subtraction.getWidth();
			height = subtraction.getHeight();
		}

		@Override
		public void perform(int x, int y, MemoryRGB48Image input,
				MemoryRGB48Image result) {
			for (int channel = 0; channel < 3; channel ++) {
				int sample = input.getSample(channel, x, y);
				int sample2 = (int) (subtraction.getSample(channel, x, y) * scalingFactor);
				int diff = sample >= sample2 ? sample - sample2 : 0;
				int lum = (input.getSample(0, x, y)
						+ input.getSample(1, x, y)
						+ input.getSample(2, x, y)) / 3;
				int lumSubtract = (int) (scalingFactor * (subtraction.getSample(0, x, y)
						+ subtraction.getSample(1, x, y)
						+ subtraction.getSample(2, x, y)) / 3);
				if (correctLuminosity) {
					double mult1 = (double)MAXVALUE / (MAXVALUE - sample2);
					double mult2 = (double)(lum - lumSubtract) / (MAXVALUE - lumSubtract);
					mult2 = mult2 < 0 ? 0 : mult2;
					double multiplier = mult1 * mult2;
					diff *= multiplier;
				}
				result.putSample(channel, x, y, diff);
			}
		}

	}
}
