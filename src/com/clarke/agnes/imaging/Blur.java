package com.clarke.agnes.imaging;

import net.sourceforge.jiu.data.MemoryRGB48Image;

public class Blur extends ImageMaker {

	private final int blurRadius;
	private final int blurStep;

	public Blur(MemoryRGB48Image image) {
		blurRadius = (image.getHeight() + image.getWidth()) / (2 * 40 * 4);
		blurStep = 3;
	}

	public Blur(int blurRadius, int blurStep) {
		this.blurRadius = blurRadius;
		this.blurStep = blurStep;
	}
	
	@Override
	public MemoryRGB48Image process(MemoryRGB48Image input) {
		BlurAction action = new BlurAction(input, blurRadius, blurStep);
		return processTemplate(input, action);
	}
	
	private static class BlurAction implements ImageMakerAction {

		private int width;
		private int height;
		private int blurRadius;
		private int blurStep = 5;

		public BlurAction(MemoryRGB48Image image, int blurRadius, int blurStep) {
			this.blurRadius = blurRadius;
			this.blurStep = blurStep;
			width = image.getWidth();
			height = image.getHeight();
		}

		@Override
		public void perform(int x, int y, MemoryRGB48Image input,
				MemoryRGB48Image result) {
			int[] sampleValueSum = new int[3];
			int sampleCount = 0;
			for (int i = -blurRadius; i <= blurRadius; i += blurStep) {
				for (int j = -40; j <= 40; j += 5) {
					int xOff = x + i;
					int yOff = y + j;
					if (xOff > 0 && yOff > 0 && xOff < width && yOff < height) {
						sampleCount++;
						for (int channel = 0; channel < 3; channel++) {
							sampleValueSum[channel] += input.getSample(channel, xOff, yOff);
						}
					}
				}
			}
			for (int channel = 0; channel < 3; channel++) {
				result.putSample(channel, x, y, sampleValueSum[channel] / sampleCount);
			}
		}

	}
	
}
