package com.clarke.agnes.imaging;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import net.sourceforge.jiu.codecs.CodecMode;
import com.clarke.agnes.imaging.TIFFCodec;
import net.sourceforge.jiu.data.MemoryRGB48Image;
import net.sourceforge.jiu.data.PixelImage;
import net.sourceforge.jiu.ops.MissingParameterException;
import net.sourceforge.jiu.ops.OperationFailedException;

public class NoiseAnalyser {

	public static void main(String[] args) throws IOException, MissingParameterException, OperationFailedException {
		long startTime = System.currentTimeMillis();
		File directory = new File("G:\\gina");
		List<ImageData> results = new LinkedList<ImageData>();
		File[] listFiles = directory.listFiles();
		ImageLooper looper = new ImageLooper();
		looper.setup(listFiles);
		while (looper.hasNext()) {
			ArrayList<MemoryRGB48Image> stack = new ArrayList<MemoryRGB48Image>();
            ArrayList<ImageData> dataStack = new ArrayList<ImageData>();
			File next = looper.sniffNext();
			String endsWith = next.getName().substring(27);
			System.out.println("Doing group: " + endsWith);
			List<File> nextGroup = looper.nextMatching(endsWith);
			if (nextGroup.size() > 2) {
				for (File f : nextGroup) {
					loadAndAnalyze(results, stack, dataStack, f);
				}
				MedianStack medianStack = new MedianStack(
						stack.toArray(new MemoryRGB48Image[stack.size()]));
				MemoryRGB48Image median = medianStack.process(null);
				ImageData lastData = results.get(results.size() - 1);
				int iso = lastData.getIso();
				int sec = lastData.getSec();
				int temp = lastData.getTemp();
				ImageData data = analyzeImage("median", iso, sec, temp,
						directory, median);
				data.writeFile();
				results.add(data);
				ImageData average = new ImageData(directory, "average", iso,
						sec, temp, dataStack);
				results.add(average);
			}
		}
		long time = (System.currentTimeMillis() - startTime) / 1000;
		System.out.println("Took " + time + " seconds.");
		Collections.sort(results, new ImageDataComparator());
		System.out.println("TYPE,ISO,SECONDS,TEMPERATURE,ALL_NOISE,LOWEST_NOISE,BAD_NOISE");
		for (ImageData datum : results) {
			if (!datum.name().equals("")) {
				System.out.println(datum.name() + "," + datum.getIso() + ","
						+ datum.getSec() + "," + datum.getTemp() + ","
						+ datum.allNoiseCounter + ","
						+ datum.lowestNoiseCounter + ","
						+ datum.badNoiseCounter);
			}
		}
		System.out.println("TYPE,ISO,SECONDS,TEMPERATURE,0.1%,0.3%,0.6%,1%,2%,3%,5%,8%,11%,16%,22%,30%,40%,52%,66%,100%");
		for (ImageData datum : results) {
			if (!datum.name().equals("")) {
				System.out.print(datum.name() + ",");
				System.out.print(datum.getIso() + "," + datum.getSec() + ","
						+ datum.getTemp() + ",");
				System.out.print(datum.getDeciPercentileDistributionRange(0, 0,
						1) + ",");
				System.out.print(datum.getDeciPercentileDistributionRange(0, 2,
						3) + ",");
				System.out.print(datum.getDeciPercentileDistributionRange(0, 4,
						6) + ",");
				System.out.print(datum.getDeciPercentileDistributionRange(0, 7,
						10) + ",");
				System.out.print(datum.getDeciPercentileDistributionRange(0,
						11, 20) + ",");
				System.out.print(datum.getDeciPercentileDistributionRange(0,
						21, 30) + ",");
				System.out.print(datum.getDeciPercentileDistributionRange(0,
						31, 50) + ",");
				System.out.print(datum.getDeciPercentileDistributionRange(0,
						51, 80) + ",");
				System.out.print(datum.getDeciPercentileDistributionRange(0,
						81, 110) + ",");
				System.out.print(datum.getDeciPercentileDistributionRange(0,
						111, 160) + ",");
				System.out.print(datum.getDeciPercentileDistributionRange(0,
						161, 220) + ",");
				System.out.print(datum.getDeciPercentileDistributionRange(0,
						221, 300) + ",");
				System.out.print(datum.getDeciPercentileDistributionRange(0,
						301, 400) + ",");
				System.out.print(datum.getDeciPercentileDistributionRange(0,
						401, 520) + ",");
				System.out.print(datum.getDeciPercentileDistributionRange(0,
						521, 660) + ",");
				System.out.print(datum.getDeciPercentileDistributionRange(0,
						661, 1000));
				System.out.println();
			}
		}
	}

	public static void loadAndAnalyze(List<ImageData> results, ArrayList<MemoryRGB48Image> stack, ArrayList<ImageData> dataStack, File tiff) {
		if (tiff.getName().endsWith(".TIF")) {
			String name = tiff.getName();
			System.out.println(name);
			String[] tiffBits = name.replace(".TIF", "").replace("__", "_").split("_");
			int iso = Integer.valueOf(tiffBits[4].replace("ISO", ""));
			int sec = Integer.valueOf(tiffBits[5].replace("s", ""));
			int temp = Integer.valueOf(tiffBits[6].replace("C", ""));
			try {
				TIFFCodec codec = new TIFFCodec();
				codec.setFile(tiff, CodecMode.LOAD);
				codec.process();
				PixelImage image = codec.getImage();
				MemoryRGB48Image rgb48Image = (MemoryRGB48Image) image;
				stack.add(rgb48Image);
				ImageData data = analyzeImage(name, iso, sec, temp, tiff.getParentFile(), rgb48Image);
				data.writeFile();
				results.add(data);
				dataStack.add(data);
			} catch (Exception e) {
				System.out.println("Could not process file: " + name + " because " + e.getMessage());
			}
		}
	}

	public static ImageData analyzeImage(String name, int iso, int sec, int temp, File file, MemoryRGB48Image image) {
		ImageData data = new ImageData(file, name, iso, sec, temp);
		TreeMap<Integer,Integer> rDistribution = new TreeMap<Integer,Integer>();
		TreeMap<Integer,Integer> gDistribution = new TreeMap<Integer,Integer>();
		TreeMap<Integer,Integer> bDistribution = new TreeMap<Integer,Integer>();
		TreeMap<Integer,Integer> mDistribution = new TreeMap<Integer,Integer>();

		int height = image.getHeight();
		int width = image.getWidth();
		int lowestNoiseCounter = 0;
		int allNoiseCounter = 0;
		int badNoiseCounter = 0;
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				int rSample = image.getSample(0, x, y);
				int r = rSample / (Short.MAX_VALUE / 500);
				int gSample = image.getSample(1, x, y);
				int g = gSample / (Short.MAX_VALUE / 500);
				int bSample = image.getSample(2, x, y);
				int b = bSample / (Short.MAX_VALUE / 500);
				int m = (r + g + b) / 3;
				lowestNoiseCounter = lowestNoise(iso, lowestNoiseCounter, rSample, gSample, bSample);
				if (rSample > 0 || gSample > 0 || bSample > 0) {
					allNoiseCounter++;
				}
				if (rSample > Short.MAX_VALUE || gSample > Short.MAX_VALUE || bSample > Short.MAX_VALUE) {
					badNoiseCounter++;
				}
				int rCount = rDistribution.get(r) == null ? 1 : rDistribution.get(r) + 1;
				int gCount = gDistribution.get(g) == null ? 1 : gDistribution.get(g) + 1;
				int bCount = bDistribution.get(b) == null ? 1 : bDistribution.get(b) + 1;
				int mCount = mDistribution.get(m) == null ? 1 : mDistribution.get(m) + 1;
				rDistribution.put(r, rCount);
				gDistribution.put(g, gCount);
				bDistribution.put(b, bCount);
				mDistribution.put(m, mCount);
			}
		}
		data.lowestNoiseCounter = lowestNoiseCounter;
		data.allNoiseCounter = allNoiseCounter;
		data.badNoiseCounter = badNoiseCounter;
		for (int i = 0; i < 1001; i++) {
			data.setDeciPercentileDistribution(i, 
					denull(rDistribution.get(i)), 
					denull(gDistribution.get(i)), 
					denull(bDistribution.get(i)), 
					denull(mDistribution.get(i)));
		}

		//field illumination
		TreeMap<Integer,Integer> illumination = new TreeMap<Integer,Integer>();
		TreeMap<Integer,Integer> illuminationAverager = new TreeMap<Integer,Integer>();
		for (int x = 0; x < width; x++) {
			int y = height * x / width;
			int x1 = width - x - 1;
			int r = (image.getSample(0, x, y));
			int g = (image.getSample(1, x, y));
			int b = (image.getSample(2, x, y));
			int m = (r + g + b) / 3;
			int r1 = (image.getSample(0, x1, y));
			int g1 = (image.getSample(1, x1, y));
			int b1 = (image.getSample(2, x1, y));
			int m1 = (r1 + g1 + b1) / 3;
			int distanceFromCenter = distanceFromCenter(x, width);
			int mIlluminationAverager = illuminationAverager.get(distanceFromCenter) == null ? 2 : illuminationAverager.get(distanceFromCenter) + 2;
			int mIllumination = illumination.get(distanceFromCenter) == null ? m + m1 : illumination.get(distanceFromCenter) + m + m1;
			illuminationAverager.put(distanceFromCenter, mIlluminationAverager);
			illumination.put(distanceFromCenter, mIllumination);
		}
		data.setIllumination(illumination, illuminationAverager);
		return data;
	}

	private static int denull(Integer integer) {
		return integer == null ? 0 : integer;
	}

	public static int lowestNoise(int ISO, int lowestNoiseCounter, int r, int g, int b) {
		int sample = r > g ? r : g;
		sample = sample > b ? sample : b;
		if (sample <= (32 * (ISO / 100)) && sample != 0) {
			lowestNoiseCounter++;
		}
		return lowestNoiseCounter;
	}

	public static int distanceFromCenter(int x, int width) {
		return Math.abs(((200 * x) / width) - 100);
	}

	public static class ImageLooper {
		LinkedList<File> filesList = new LinkedList<File>();

		public void addFiles(File[] filesToAdd) {
			System.out.println("Adding files to loop: " + filesToAdd.length + " + " + filesList.size() + " = " + (filesToAdd.length + filesList.size()));
			filesList.addAll(Arrays.asList(filesToAdd));
		}

		public void setup(File[] listFiles) {
			for (File f : listFiles) {
				if (f.isDirectory()) {
					setup(f.listFiles());
				} else if (f.getName().endsWith(".TIF")){
					filesList.add(f);
				}
			}
			
		}

		public List<File> nextMatching(String endsWith) {
			System.out.println(filesList.size());
			List<File> resultList = new LinkedList<File>();
			java.util.Iterator<File> it = filesList.iterator();
			while (it.hasNext()) {
				File f = it.next();
				if (f.getName().endsWith(endsWith)) {
					resultList.add(f);
					it.remove();
				}
			}
			System.out.println("After " + filesList.size());
			return resultList;
		}

		public boolean hasNext() {
			return !filesList.isEmpty();
		}

		public File sniffNext() {
			return filesList.get(0);
		}

	}

}
