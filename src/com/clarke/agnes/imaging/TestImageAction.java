package com.clarke.agnes.imaging;

import java.io.File;
import java.io.IOException;

import net.sourceforge.jiu.codecs.CodecMode;
import net.sourceforge.jiu.codecs.InvalidFileStructureException;
import net.sourceforge.jiu.codecs.PNGCodec;
import net.sourceforge.jiu.codecs.UnsupportedCodecModeException;
import net.sourceforge.jiu.codecs.UnsupportedTypeException;
import net.sourceforge.jiu.codecs.WrongFileFormatException;
import net.sourceforge.jiu.data.MemoryRGB48Image;
import net.sourceforge.jiu.data.PixelImage;
import net.sourceforge.jiu.ops.MissingParameterException;
import net.sourceforge.jiu.ops.OperationFailedException;

@SuppressWarnings("ConstantConditions")
public class TestImageAction {

	public static void main(String[] args) {
		File directory = new File("C:\\carey_env\\workspace2\\image_processing\\src\\com\\clarke\\agnes\\imaging\\dome");
		for (File tiff : directory.listFiles()) {
			if (tiff.getName().endsWith(".tiff")) {
				try {
					TIFFCodec codec = new TIFFCodec();
					codec.setFile(tiff, CodecMode.LOAD);
					codec.process();
					PixelImage image = codec.getImage();
//                    MemoryRGB24Image img24 = (MemoryRGB24Image) image;
//                    MemoryRGB48Image img2 = new MemoryRGB48Image(img24.getWidth(), img24.getHeight());
//                    for (int x = 0; x < img24.getWidth(); x++) {
//                        for (int y = 0; y < img24.getHeight(); y++) {
//                            int r = img24.getSample(0, x, y);
//                            int g = img24.getSample(1, x, y);
//                            int b = img24.getSample(2, x, y);
//                            img2.putSample(0, x, y, r * 256);
//                            img2.putSample(1, x, y, g * 256);
//                            img2.putSample(2, x, y, b * 256);
//                        }
//                    }
//                    MemoryRGB48Image image48 = img2;
//                    writePng(directory, tiff, image48, "_16bitDepth.png");
					MemoryRGB48Image image48 = (MemoryRGB48Image) image;

					MemoryRGB48Image avgImg = extractGradient(directory, tiff,
							image48);

					Subtract subtract = new Subtract(avgImg, 1.0, false);
					Subtract subtract2 = new Subtract(avgImg, 1.0, true);

					MemoryRGB48Image subtracted = subtract.process(image48);
					writePng(directory, tiff, subtracted, "_subtracted.png");

					Stretch stretch = new Stretch(100.0);
					avgImg = stretch.process(subtracted);
					writePng(directory, tiff, avgImg, "_subtracted_stretched.png");

					subtracted = subtract2.process(image48);
					writePng(directory, tiff, subtracted, "_subtracted_correct_luminosity.png");

					avgImg = stretch.process(subtracted);
					writePng(directory, tiff, avgImg, "_subtracted_correct_luminosity_stretched.png");

					avgImg = stretch.process(avgImg);
					writePng(directory, tiff, avgImg, "_subtracted_correct_luminosity_stretched2.png");

					//MemoryRGB48Image gradient2 = extractGradient(directory, tiff, avgImg);

					//Subtract subtract3 = new Subtract(gradient2, 1.0, false);
					//avgImg = subtract3.process(avgImg);
					//writePng(directory, tiff, avgImg, "_final.png");


				} catch (Exception e) {
					System.out.println("Could not process file: " + tiff.getName() + " because " + e.getMessage());
				}
			}
		}
	}

	public static MemoryRGB48Image extractGradient(File directory, File tiff,
			MemoryRGB48Image image48) throws IOException, OperationFailedException,
			WrongFileFormatException {
		Blur preBlur = new Blur(15, 5);
		MemoryRGB48Image preBlurred = preBlur.process(image48);
		writePng(directory, tiff, preBlurred, "_preblurred.png");

		GradientRemover gr = new GradientRemover();
		MemoryRGB48Image avgImg = gr.process(preBlurred);
		writePng(directory, tiff, avgImg, "_background.png");

		Blur blur = new Blur(avgImg);
		avgImg = blur.process(avgImg);
		writePng(directory, tiff, avgImg, "_background_blur.png");

		avgImg = blur.process(avgImg);
		writePng(directory, tiff, avgImg, "_background_double_blur.png");

		avgImg = blur.process(avgImg);
		writePng(directory, tiff, avgImg, "_background_triple_blur.png");
		return avgImg;
	}

	public static int outputCounter = 0;

	public static void writePng(File directory, File tiff,
			MemoryRGB48Image avgImg, String extension) throws IOException,
			OperationFailedException {
        try {
            PNGCodec pngCodec = new PNGCodec();
            pngCodec.setFile(new File(directory, tiff.getName().replace(".tiff", (outputCounter++ + '_' + extension))), CodecMode.SAVE);
            pngCodec.setImage(avgImg);
            pngCodec.process();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

}
