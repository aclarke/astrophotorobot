package com.clarke.agnes.imaging;

import com.clarke.agnes.imaging.GradientRemover.BlockValues;

public class ReflectedBlock extends BlockValues {

	private final BlockValues center;
	private final BlockValues reflect;

	public ReflectedBlock(BlockValues center, BlockValues reflect) {
		super(0,0,0,0);
		this.center = center;
		this.reflect = reflect;
	}
	
	@Override
	public int getW() {
		int c = center.getW();
		int r = reflect.getW();
		int delta = c - r;
		return c + delta;
	}

	@Override
	public int getH() {
		int c = center.getH();
		int r = reflect.getH();
		int delta = c - r;
		return c + delta;
	}

	@Override
	public int getLowest(int channel) {
		int centerLow = center.getLowest(channel);
		int reflectLow = reflect.getLowest(channel);
		int delta = centerLow - reflectLow;
		return centerLow + delta;
	}
}
