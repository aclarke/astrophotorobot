package com.clarke.agnes.imaging.astrophotorobot;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class LoadImage {

    public static final LoadImage INSTANCE = new LoadImage();

    public BufferedImage loadImage(File input) throws IOException {
        return ImageIO.read(input);
    }

}
