package com.clarke.agnes.imaging.astrophotorobot;

import java.awt.image.BufferedImage;

public class PixelIterator {

    public static void iterate(BufferedImage in, PixelIteratorAction action) {
        for (int x = 0; x < in.getWidth(); x++) {
            for (int y = 0; y < in.getHeight(); y++) {
                action.action(in, x, y);
            }
        }
    }

    /**Sometimes extreme edges need special processing.*/
    @SuppressWarnings("UnusedDeclaration")
    public static void iterate(BufferedImage in, PixelIteratorAction action, int inset) {
        for (int x = inset; x < in.getWidth() - inset; x++) {
            for (int y = inset; y < in.getHeight() - inset; y++) {
                action.action(in, x, y);
            }
        }
    }

    public static interface PixelIteratorAction {

        public void action(BufferedImage in, int x, int y);

    }

}
