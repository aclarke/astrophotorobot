package com.clarke.agnes.imaging.astrophotorobot;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;

public class CopyImage {

    public static final CopyImage INSTANCE = new CopyImage();
    private static final double FACTOR = 0.6;

    public void copyImage(File input, File output, String formatName) {
        try {
            BufferedImage image = ImageIO.read(input);
            System.out.println(image.getColorModel().getComponentSize()[0]);
            System.out.println(image.getColorModel().getComponentSize().length);
            Raster r = image.getRaster();
            WritableRaster wr = r.createCompatibleWritableRaster();
            image.copyData(wr);
            for (int x = 0; x < wr.getWidth(); x++) {
                for (int y = 0; y < wr.getHeight(); y++) {
                    int[] rgb = wr.getPixel(x, y, new int[3]);
                    if (rgb[0] * FACTOR > Short.MAX_VALUE && rgb[1] * FACTOR > Short.MAX_VALUE && rgb[2] * FACTOR > Short.MAX_VALUE) {
                        rgb[0] = Short.MAX_VALUE * 2 - 1;
                        rgb[1] = 0;
                        rgb[2] = 0;
                    }
                    wr.setPixel(x, y, rgb);
                }
            }
            image.setData(wr);
            ImageIO.write(image, formatName, output);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("UnusedDeclaration")
    public static void main(String[] args) {
        File inPNG = new File("C:\\carey_env\\workspace2\\image_processing\\src\\com\\clarke\\agnes\\imaging\\astrophotorobot\\favicon48.png");
        File outPNG = new File("C:\\carey_env\\workspace2\\image_processing\\src\\com\\clarke\\agnes\\imaging\\astrophotorobot\\out.png");
        File inJPEG = new File("C:\\carey_env\\workspace2\\image_processing\\src\\com\\clarke\\agnes\\imaging\\astrophotorobot\\IMG_7875color2.JPG");
        File outJPEG = new File("C:\\carey_env\\workspace2\\image_processing\\src\\com\\clarke\\agnes\\imaging\\astrophotorobot\\out.JPG");
        File inTIFF = new File("C:\\carey_env\\workspace2\\image_processing\\src\\com\\clarke\\agnes\\imaging\\astrophotorobot\\m45_light_1.TIF");
        File inTIFF2 = new File("C:\\carey_env\\workspace2\\image_processing\\src\\com\\clarke\\agnes\\imaging\\astrophotorobot\\nan-dss-stretch.tiff");
        File inTIFF3 = new File("C:\\carey_env\\workspace2\\image_processing\\src\\com\\clarke\\agnes\\imaging\\astrophotorobot\\nan-initial.tiff");
        File outTIFF = new File("C:\\carey_env\\workspace2\\image_processing\\src\\com\\clarke\\agnes\\imaging\\astrophotorobot\\out.TIF");

        //CopyImage.INSTANCE.copyImage(inPNG, outPNG, "png");
        //CopyImage.INSTANCE.copyImage(inJPEG, outJPEG, "jpeg");
        //CopyImage.INSTANCE.copyImage(inJPEG, outTIFF, "tiff");//works
        CopyImage.INSTANCE.copyImage(inTIFF3, outTIFF, "tiff");//works
        //CopyImage.INSTANCE.copyImage(inTIFF, outPNG, "png");//works, slow

    }

}
