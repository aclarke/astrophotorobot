package com.clarke.agnes.imaging.astrophotorobot;

import java.awt.image.BufferedImage;

/**
* Created by agslinda on 2/10/14.
*/
@SuppressWarnings("UnusedDeclaration")
public class BlurMask {
    private final int[][] blurMask;
    private final int[][] blurMaskR;
    private final int[][] blurMaskG;
    private final int[][] blurMaskB;
    private final int[][] blurMaskA;
    private final int blurDiameter;
    private final int blurRadius;
    private final BufferedImage image;

    public BlurMask(int blurRadius, BufferedImage image) {
        this.image = image;
        this.blurRadius = blurRadius;
        blurDiameter = this.blurRadius * 2 + 1;
        blurMask = new int[blurDiameter][blurDiameter];
        blurMaskR = new int[blurDiameter][blurDiameter];
        blurMaskG = new int[blurDiameter][blurDiameter];
        blurMaskB = new int[blurDiameter][blurDiameter];
        blurMaskA = new int[blurDiameter][blurDiameter];

        float bRadius = blurRadius + 0.5f;
        for (int y = -blurRadius; y < blurRadius + 1; y++) {
            for (int x = -blurRadius; x < blurRadius + 1; x++) {
                float hypotenuse = (float) Math.sqrt(y * y + x * x);
                blurMask[y + blurRadius][x + blurRadius] =
                        (hypotenuse < bRadius) ? 1 : 0;
            }
        }

    }

    public AgsColor blur(int x, int y) {
        return null;
    }

    public static void main(String[] args) {
        BlurMask bm = new BlurMask(13, null);
        for (int[] arr : bm.blurMask) {
            for (int pixel : arr) {
                System.out.print(pixel == 1 ? '%' : ' ');
            }
            System.out.println();
        }
    }
}
