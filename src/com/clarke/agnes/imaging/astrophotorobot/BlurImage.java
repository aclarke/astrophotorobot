package com.clarke.agnes.imaging.astrophotorobot;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;

public class BlurImage {

    public static final BlurImage INSTANCE = new BlurImage();

    public BufferedImage blur(BufferedImage input, int blurRadius) {
        final BufferedImage output = new BufferedImage(input.getWidth(), input.getHeight(), input.getType());
        Raster r = input.getRaster();
        final WritableRaster wr = r.createCompatibleWritableRaster();
        //input.copyData(wr);
        final BlurMask mask = new BlurMask(blurRadius, input);
        PixelIterator.iterate(input, new PixelIterator.PixelIteratorAction() {
            @Override
            public void action(BufferedImage in, int x, int y) {
                AgsColor rgb = mask.blur(x, y);
                wr.setSample(x, y, 0, rgb.r);
                wr.setSample(x, y, 1, rgb.g);
                wr.setSample(x, y, 2, rgb.b);
            }
        });
        output.setData(wr);
        return output;
    }

    public static void main(String[] args) throws IOException {
        BufferedImage img = LoadImage.INSTANCE.loadImage(new File("C:\\carey_env\\workspace2\\image_processing\\src\\com\\clarke\\agnes\\imaging\\dome\\M66-Leo-triplet-stacked-1-flat-crop1 - 16-bit-rgb100_subtracted.png"));
        BufferedImage out = BlurImage.INSTANCE.blur(img, 12);
        ImageIO.write(out, "png", new File("C:\\carey_env\\workspace2\\image_processing\\src\\com\\clarke\\agnes\\imaging\\astrophotorobot\\outblur.png"));
    }

}
