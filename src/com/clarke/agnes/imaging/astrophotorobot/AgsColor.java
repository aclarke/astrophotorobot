package com.clarke.agnes.imaging.astrophotorobot;

public class AgsColor {

    public final int r;
    public final int g;
    public final int b;
    public final int a;

    public AgsColor(int r, int g, int b, int a) {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
    }

}
